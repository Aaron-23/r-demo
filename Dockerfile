#FROM rocker/shiny
FROM goodrain.me/shiny
COPY ./app /srv/shiny-server/ 
VOLUME /var/log/shiny-server/
EXPOSE 3838
